//SimpleCanvas v21_10_14
//_functions are private

var SC = (function () {
    var _canvas;
    var _context;
    var _height;
    var _width;
    //get and set properties of the canvas
    function init(_id, _w, _h) {
        var c = document.createElement('canvas');
        document.body.appendChild(c);

        c.id = _id;
        c.width = _w;
        c.height = _h;
        c.style.border = "1px solid";
        c.style.margin = "0 auto";
        c.style.display = "block";
        _canvas = c;
        _context = c.getContext("2d");
        _height = _h;
        _width = _w;
    }

    function get() {
        return {
            canvas: _canvas,
            context: _context,
            width: _width,
            height: _height
        }
    }
    //Utils BEGIN
    var utils = {
            clear: function (_color) {
                _context.fillStyle = _color;
                _context.fillRect(0, 0, _width, _height);
            }
        }
        //Utils END

    //SHAPES BEGIN
    var shapes = {
            point: function (_x, _y, _color, _stroke) {
                _context.fillStyle = _color;
                _context.fillRect(_x, _y, _stroke, _stroke);
            },
            rect: function (_x, _y, _width, _height, _color) {
                _context.fillStyle = _color;
                _context.fillRect(_x, _y, _width, _height);
            }
        }
        //SHAPES END

    //static sprite BEGIN 
    function _createImage(_imagepath) {
        var image = new Image();
        image.src = _imagepath;
        return image;
    }

    function staticSprite(_imgpath, _width, _height) {
        this.image = _createImage(_imgpath);
        this.width = _width;
        this.height = _height;
        this.pivot = {
            x: 0.5,
            y: 0.5
        };
        this.count = 0;
        this.angle = [];
        this.alpha = [];
        this.instance = [];
        this.imagePoints = [];
        this.maxDrawDistance = {
            x: 5000,
            y: 5000
        }
    }

    staticSprite.prototype.drawStaticSprite = function () {
        for (_i = 0; _i < this.count; _i++) {
            if (this.instance[_i].x < (-1 * this.maxDrawDistance.x) || this.instance.x > this.maxDrawDistance.x || this.instance[_i].y < (-1 * this.maxDrawDistance.y) || this.instance.y > this.maxDrawDistance.y) {
                this.removeInstance(_i);
            }
        }

        for (_i = 0; _i < this.count; _i++) {
            _context.save();
            _context.translate(this.instance[_i].x, this.instance[_i].y);
            _context.translate(this.width * this.pivot.x, this.height * this.pivot.y);
            _context.rotate(this.angle[_i] * Math.PI / 180);
            _context.globalAlpha = this.alpha[_i];
            _context.drawImage(this.image, -(this.width * this.pivot.x), -(this.height * this.pivot.y));
            //_context.drawImage(this.image, this.instance[_i].x, this.instance[_i].y);
            _context.globalAlpha = 1;
            _context.restore();
        }
    }

    staticSprite.prototype.createInstance = function (_x, _y, _angle, _alpha) {
        this.instance.push({
            'x': _x,
            'y': _y
        });
        this.angle.push(_angle); //default should be 0
        this.alpha.push(_alpha); //default should be 1
        this.count++;
    }

    staticSprite.prototype.setPivot = function (_x, _y) {
        this.pivot.x = _x;
        this.pivot.y = _y;
    }

    staticSprite.prototype.addImagePoint = function (_x, _y) {
        this.imagePoints.push({
            'x': _x,
            'y': _y
        });
    }

    staticSprite.prototype.removeInstance = function (_i) {
        this.instance.splice(_i, 1);
        this.angle.splice(_i, 1);
        this.alpha.splice(_i, 1);
        this.count--;
    }

    staticSprite.prototype.setMaxDrawDistance = function (_x, _y) {
        this.maxDrawDistance.x = _x;
        this.maxDrawDistance.y = _y;
    }
    //staticSprite END
    //Keyboard Begin
    function KB() {
        this.keys = new Array();
        for (_i = 0; _i < 256; _i++)
            this.keys[_i] = 0;

        window.addEventListener('keydown', this.setKeyDown.bind(this), false);
        window.addEventListener('keyup', this.setKeyUp.bind(this), false);
    }

    KB.prototype.isKeyDown = function (_keyCode) {
        //console.log(_keyCode," ", self.keys[_keyCode]);
        if (this.keys[_keyCode])
            return 1;
        else
            return 0;

    }

    KB.prototype.setKeyDown = function (_e) {
        this.keys[_e.keyCode] = 1;

    }

    KB.prototype.setKeyUp = function (_e) {
        this.keys[_e.keyCode] = 0;
    }
    //Keyboard END
    function Mouse() {

        this.lmb = 0;
        this.mmb = 0;
        this.rmb = 0;
        this.mouseX = 0;
        this.mouseY = 0;
        window.addEventListener('mousemove', this.getMousePos.bind(this), false);
        window.addEventListener('mousedown', this.setMouseDown.bind(this), false);
        window.addEventListener('mouseup', this.setMouseUp.bind(this), false);
    }
    Mouse.prototype.getMousePos = function (_e) {
        var _rect = _canvas.getBoundingClientRect();
        this.mouseX = _e.clientX - _rect.left;
        this.mouseY = _e.clientY - _rect.top;
    }
    Mouse.prototype.isMouseDown = function (_button) {
        if (_button == 1) {
            if (this.lmb == 1)
                return 1;
            else
                return 0;
        }
        if (_button == 2) {
            if (this.mmb == 1)
                return 1;
            else
                return 0;
        }
        if (_button == 3) {
            if (this.rmb == 1)
                return 1;
            else
                return 0;
        }
    }
    Mouse.prototype.setMouseDown = function (_e) {
        //console.log("e: ",_e.which);
        if (_e.which == 1)
            this.lmb = 1;
        if (_e.which == 2)
            this.mmb = 1;
        if (_e.which == 3)
            this.rmb = 1;
    }
    Mouse.prototype.setMouseUp = function (_e) {
        if (_e.which == 1)
            this.lmb = 0;
        if (_e.which == 2)
            this.mmb = 0;
        if (_e.which == 3)
            this.rmb = 0;
    }
    var collision = {
        boxCollision: function (_sprite1, _i, _sprite2, _j) {
            var respose = new SAT.Response();
            var s1Box = new SAT.Box(new SAT.Vector(_sprite1.instance[_i].x, _sprite1.instance[_i].y), _sprite1.width, _sprite1.height);
            var s2Box = new SAT.Box(new SAT.Vector(_sprite2.instance[_j].x, _sprite2.instance[_j].y), _sprite2.width, _sprite2.height);
            var collided = SAT.testPolygonPolygon(s1Box.toPolygon(), s2Box.toPolygon(), respose);
            return {
                collided: collided,
                reponse: respose
            };
        }

    }

    function text(_font, _text, _x, _y, _size, _color) {
        this.font = _font;
        this.text = _text;
        this.x = _x;
        this.y = _y;
        this.size = _size;
        this.color = _color;
    }

    text.prototype.drawText = function () {
        _context.save();
        _context.font = this.size + "px " + this.font;
        _context.fillStyle = this.color;
        _context.fillText(this.text, this.x, this.y);
        _context.restore();
    }


    //final return
    return {
        init: init,
        get: get,
        utils: utils,
        staticSprite: staticSprite,
        KB: KB,
        Mouse: Mouse,
        shapes: shapes,
        collision: collision,
        text: text
    }
})();